// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';

void main() {
  Wakelock.enable();
  runApp(MyApp());
}

Map<String, int> time = {
  '6': 10,
  '5': 15,
  '4': 20,
  '3': 30,
  '2': 40,
  '1': 50,
  '0': 60,
  '00': 120
};

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Rallyman GT',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      home: SoloPage(),
    );
  }
}

class SoloPage extends StatefulWidget {
  const SoloPage({Key? key}) : super(key: key);

  @override
  State<SoloPage> createState() => _SoloPageState();
}

class _SoloPageState extends State<SoloPage> {
  List<String> turns = [];
  int focus = 0;
  int? turnTarget;

  bool isTurnHighlighted(int i) => turnTarget == null || turnTarget == i;

  TextEditingController controller = TextEditingController();

  String getTime() => intToTimeLeft((turns.isNotEmpty ? turns.map((e) => time[e]).reduce((v, e) => v! + e!) ?? 0 : 0) - focus);

  void reset() => setState(() {
    turns = [];
    focus = 0;
    turnTarget = null;
    controller.clear();
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rallyman GT'),
        actions: [
          turnTarget != null ? IconButton(
            icon: Icon(Icons.delete),
            onPressed: () => setState(() {
              turns.removeAt(turnTarget!);
              turnTarget = null;
            }),
          ) : Container(),
          IconButton(
            icon: Icon(Icons.restart_alt),
            onPressed: () => reset(),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: Colors.orange.shade300,
        onPressed: () async {
          int focusPoints = int.tryParse(await showDialog(context: context, builder: (context) => AlertDialog(
            title: Text('Nombre de points de focus :'),
            content: TextField(
              keyboardType: TextInputType.phone,
              autofocus: true,
              controller: controller,
            ),
            actions: [
              TextButton(
                child: Text('Enregistrer'),
                onPressed: () => Navigator.of(context).pop(controller.text),
              )
            ],
          ))) ?? 0;

          setState(() => focus = focusPoints);
        },
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Container(
              padding: EdgeInsets.only(top: 4),
              child: Icon(Icons.flash_on, size: 36,)
            ),
            Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(bottom: 4),
              child: Text('x$focus', style: TextStyle(fontSize: 16),)
            )
          ],
        ),
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   items: [
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.ac_unit_outlined),
      //       label: 'Solo',
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.ac_unit_outlined),
      //       label: 'Solo',
      //     )
      //   ]
      // ),
      body: Column(
        children: [
          DefaultTextStyle.merge(
            style: TextStyle(fontFamily: 'Digits'),
            child: Container(
              constraints: BoxConstraints.expand(height: 96),
              padding: EdgeInsets.symmetric(vertical: 24, horizontal: 12),
              child: Wrap(
                spacing: 6,
                runSpacing: 6,
                children: List.generate(turns.length, (i) => GestureDetector(
                  onTap: () => setState(() => turnTarget = turnTarget == i ? null : i),
                  child: Container(
                      height: 46,
                      width: 46,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: isTurnHighlighted(i) ? Colors.white : Colors.grey.shade600,
                        border: Border.all(color: isTurnHighlighted(i) ? Colors.deepPurple : Colors.deepPurple.shade800, width: 5)
                      ),
                      child: Center(child: Text(turns[i], style: TextStyle(fontSize: 18, color: Colors.black)))
                    ),
                )
                )
              ),
            ),
          ),
          Expanded(
            child: DefaultTextStyle.merge(
              style: TextStyle(fontFamily: 'Digits'),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('1');
                          }
                          else {
                            turns[turnTarget!] = '1';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.black,
                          child: Center(child: Text('1', style: TextStyle(fontSize: 64, color: Colors.green)))
                        ),
                      ),
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('2');
                          }
                          else {
                            turns[turnTarget!] = '2';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.black,
                          child: Center(child: Text('2', style: TextStyle(fontSize: 64, color: Colors.lightGreen.shade400)))
                        ),
                      ),
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('3');
                          }
                          else {
                            turns[turnTarget!] = '3';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.black,
                          child: Center(child: Text('3', style: TextStyle(fontSize: 64, color: Colors.yellow.shade700)))
                        ),
                      ),
                    ]
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('4');
                          }
                          else {
                            turns[turnTarget!] = '4';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.black,
                          child: Center(child: Text('4', style: TextStyle(fontSize: 64, color: Colors.orange)))
                        ),
                      ),
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('5');
                          }
                          else {
                            turns[turnTarget!] = '5';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.black,
                          child: Center(child: Text('5', style: TextStyle(fontSize: 64, color: Colors.deepOrange)))
                        ),
                      ),
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('6');
                          }
                          else {
                            turns[turnTarget!] = '6';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.black,
                          child: Center(child: Text('6', style: TextStyle(fontSize: 64, color: Colors.red.shade600)))
                        ),
                      ),
                    ]
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('0');
                          }
                          else {
                            turns[turnTarget!] = '0';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.orange.shade900,
                          child: Center(child: Text('0', style: TextStyle(fontSize: 64, color: Colors.white)))
                        ),
                      ),
                      GestureDetector(
                        onTap: () => setState(() {
                          if (turnTarget == null) {
                            turns.add('00');
                          }
                          else {
                            turns[turnTarget!] = '00';
                            turnTarget = null;
                          }
                        }),
                        child: Container(
                          width: 96,
                          height: 96,
                          color: Colors.red.shade900,
                          child: Center(child: Text('00', style: TextStyle(fontSize: 58, color: Colors.white)))
                        ),
                      )
                    ]
                  ),
                ],
              ),
            )
          ),
          Container(
            height: 96,
            color: Colors.black,
            child: Center(
              child: Text(getTime(), style: TextStyle(fontSize: 24, fontFamily: 'Digits'),)
            )
          )
        ],
      ),
    );
  }
}

String intToTimeLeft(int value) {
  int m, s;

  m = value ~/ 60;

  s = value - (m * 60);

  String minuteLeft =
      m.toString().length < 2 ? "0" + m.toString() : m.toString();

  String secondsLeft =
      s.toString().length < 2 ? "0" + s.toString() : s.toString();

  String result = "$minuteLeft:$secondsLeft";

  return result;
}